function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    //Answer:
    if(letter.length !== 1){
        return undefined;
    }

    const regex = new RegExp(letter, "g");
    const matches = sentence.match(regex);

    return matches ? matches.length : 0;
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    //Answer
    const letters = {};

    for (let i = 0; i < text.length; i++) {
        const letter = text[i].toLowerCase();
        if(letters[letter]) {
            return false
        }else{
            letters[letter] = true;
        }
    }
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
if (age < 13) {
    // If age is below 13, return undefined
        return undefined;
    } else if (age <= 21 || age >= 65) {
    // If age is between 13 and 21, or above 64, apply discount
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    } else {
    // Otherwise, return the original price
        return price.toFixed(2);
    }
}


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

const categories = [];
    const categoriesSet = new Set();
    for (const item of items) {
        if (item.stocks === 0 && !categoriesSet.has(item.category)) {
          categories.push(item.category);
          categoriesSet.add(item.category);
      }
    }
    return categories;

}


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
 const mapA = new Map(candidateA.map(id => [id, true]));
    const mapB = new Map(candidateB.map(id => [id, true]));
    const intersection = [];
    for (const id of mapA.keys()) {
        if (mapB.has(id)) {
            intersection.push(id);
        }
    }
    return intersection;
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};